import { Schema, model } from 'mongoose';

const BankAccount = new Schema({
    number: {
        type: Number,
        required:true
    },
    bank: {
        type:String,
        required:true
    },
    id_artist: {
        type: String,
        required: true
    }
});


export default model('bankaccount', BankAccount);