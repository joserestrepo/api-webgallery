import { Schema, model } from 'mongoose';
import bcrypt from 'bcryptjs';

const user = Schema({
    username: {
        type: String,
        required: true,
        unique:true,
    },
    email: {
        type: String,
        required: true,
        unique:true,
        lowercase:true
    },
    password: {
        type: String,
        required: true,
    },
    time_create:{
        type: Date,
        required: true,
        default: new Date()
    },
    time_update:{
        type: Date,
        required: true,
        default: new Date()
    },
    role_id: {
        type:String,
        unique:true,
        required: true
    }
});

user.pre('save', function(next){
    let user = this;
    if (!user.isModified('password')) { return next(); }
    const getSalt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(user.password, getSalt);
    user.password = hash;
    next();
})

user.methods.comparePassword = function(password, passwordCompare){
    console.log(password, passwordCompare)
    return bcrypt.compareSync(passwordCompare, password);
}

export default model('users', user)