import jwt from 'jsonwebtoken';
import { secretKeyToken } from './../config/config'

function createToken(data){
    return jwt.sign({...data}, secretKeyToken, {expiresIn: 60*60*24})
}

module.exports = {
    createToken
}