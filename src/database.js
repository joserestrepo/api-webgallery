import mongoose from 'mongoose';
import config from './config/config';

mongoose.connect(config.db,{
    useUnifiedTopology: true,
    useNewUrlParser: true
}, (err)=>{
    if(err) console.log(err);
    console.log("Connection an Database successfully");
});