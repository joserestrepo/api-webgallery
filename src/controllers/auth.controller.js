import Role from './../models/role.model';
import TypeDocument from './../models/type.document.model';
import DataPersonal from './../models/data.personal.model';
import Address from './../models/address.user.model';
import Artist from './../models/artist.model';
import BankAccount from './../models/bank.account.model';
import User from './../models/user.model';
import { createToken } from './../helpers/token.helper';

async function singIn(req, res) {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (user == null) {
            return res.status(401).json({ success: false, message: "Credenciales invalidas!" });
        }
        if (!user.comparePassword(user.password, password)) {
            return res.status(401).json({ success: false, message: "Credenciales invalidas!!!!!" });
        }
        let dataResponse = user.toJSON();
        delete dataResponse.password;
        const role = await Role.findOne({ _id: user.role_id });
        dataResponse.role_user = role.toJSON();
        return res.json({ success: true, message: "Usuario logiado", token: createToken(dataResponse) });
    } catch (error) {
        return res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error })
    }

}

async function singUp(req, res) {
    try {
        const { role_id, type_document_id } = req.body;
        const role = await Role.findOne({ _id: role_id });
        if (role == null) {
            return res.status(400).json({ success: false, message: "El rol solicitado no existe en la base de datos" })
        }

        switch (role.name) {
            case "comprador":
                const typeDocument = await TypeDocument.findOne({ _id: type_document_id });
                if (typeDocument == null) {
                    return res.status(400).json({ success: false, message: "El tipo de documento solicitado no existe en la base de datos" })
                }
                const newUser = new User({
                    ...req.body
                });
                const newDataPersonal = new DataPersonal({
                    ...req.body
                });
                const newAddress = new Address({
                    ...req.body
                });

                const user_save = await newUser.save();
                console.log(user_save)
                newDataPersonal.user_id = user_save._id;
                await newDataPersonal.save();
                newAddress.user_id = user_save._id;
                await newAddress.save();

                return res.json({ success: true, message: "La informacion del usuario ha sido guardada con exito!" });

            case "artista":
                const newUserArtist = new User({
                    ...req.body
                });
                const newArtist = new Artist({
                    ...req.body
                });
                const newDataPersonalArtist = new DataPersonal({
                    ...req.body
                });
                const newBackAccount = new BankAccount({
                    ...req.body
                });
                console.log("llego")
                const user_artist_save = await newUserArtist.save();
                newArtist.user_id = user_artist_save._id;
                await newArtist.save();
                newDataPersonalArtist.user_id = user_artist_save._id;
                await newDataPersonalArtist.save();
                newBackAccount.id_artist = user_artist_save._id;
                await newBackAccount.save();

                return res.json({ success: true, message: "La informacion del usuario ha sido guardada con exito!" });
        }


    } catch (error) {
        return res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error })
    }

}

module.exports = {
    singUp,
    singIn
}