import Role from './../models/role.model';

async function getRoles(req, res){
    try {
        const {err, roles} = Role.find();
        if(err){
            res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error });
        }
        return res.json({
            success: true,
            data: roles
        });

    } catch (error) {
        res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error });
    }
}

module.exports = {
    getRoles
}